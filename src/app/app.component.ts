import { Component } from '@angular/core';
import { CrudService } from './services/crud.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-taller';
  public users:any;
  public name:string = '';
  public last_name:string = '';
  public email:string = '';
  public description:string = '';
  public buffer:number;
  constructor(public _crudService:CrudService){

  }

  public getAllUsers(){
    this._crudService.getUsers().subscribe((data) => {
        this.users = data;
    } , (err) => {console.log(err)})
  }
  ngOnInit(){
    this.getAllUsers();
  }

  public cleanInputs(){
    this.name = '';
    this.last_name = '';
    this.email = '';
    this.description = '';
  }

  public sendUser(e){
    e.preventDefault();
    let newUser = {
      name: this.name,
      lastname: this.last_name,
      email:this.email,
      description:this.description
    }
    this._crudService.saveUser(newUser).subscribe((data) => {
      alert('Usuario guardado correctamente');
      this.cleanInputs();
      this.getAllUsers();
    } , err => { console.log(err)});
  }

  public sendUpdateUser(e){
    e.preventDefault();
    let updateUser = {
      name: this.name,
      lastname: this.last_name,
      email:this.email,
      description:this.description
    }
    this._crudService.updateUser(updateUser, this.buffer).subscribe((data) => {
      alert('Usuario actualizado correctamente');
      this.cleanInputs();
      this.getAllUsers();
    } , err => { console.log(err)});
  }

  public saveIdInBuffer(user){
    this.buffer = user.iduser;
    this.name = user.name;
    this.last_name = user.lastname;
    this.email = user.email;
    this.description = user.description
  }

  public sendDeleteUser(id){
    this._crudService.deleteUser(id).subscribe(success => {
      this.getAllUsers();
      alert('Usuario eliminado correctamente');
    } , err => console.log(err))
  }
}
