import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(public http:HttpClient) { }

  public getUsers(){
    return this.http.get('http://localhost:8083/usuarios');
  }

 // 1 - cabeceras  // permisos para hacer la peticion
 // 2 - payload  // informacion , contraseñ
 // 3 - metodo // get post put delete
  public saveUser(data){
    const headers = new HttpHeaders();
    headers.set('Content-Type','application/json'); 
    return this.http.post('http://localhost:8083/usuarios', data , { headers });
  }

  public updateUser(data, id){
    const headers = new HttpHeaders();
    headers.set('Content-Type','application/json');
    return this.http.post('http://localhost:8083/usuarios/update/'+id, data , { headers });
  }

  public deleteUser(id){
    const headers = new HttpHeaders();
    headers.set('Content-Type','application/json');
    return this.http.post('http://localhost:8083/usuarios/delete/'+id, null , {headers});
  }
}
